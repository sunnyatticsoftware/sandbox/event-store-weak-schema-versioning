using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EventStore.ClientAPI;
using EventStoreWrapper.Abstractions;
using Newtonsoft.Json;

namespace EventStoreWrapper
{
    public class EventStoreReader
        : IEventStoreReader
    {
        private readonly IEventStoreConnection _eventStoreConnection;
        private readonly IEnumerable<Type> _supportedEvents;

        public EventStoreReader(
            IEventStoreConnection eventStoreConnection,
            IEnumerable<Type> supportedEvents)
        {
            _eventStoreConnection = eventStoreConnection;
            _supportedEvents = supportedEvents;
        }
        
        public async Task<IEnumerable<object>> ReadEvents(string streamName)
        {
            if (streamName == null)
            {
                throw new ArgumentNullException(nameof(streamName));
            }
            
            var streamSlice = await _eventStoreConnection.ReadStreamEventsForwardAsync(streamName, 0, 4096, false);
            var events = GetEvents(streamSlice.Events);
            return events;
        }
        
        private IEnumerable<object> GetEvents(IEnumerable<ResolvedEvent> resolvedEvents)
        {
            var events = resolvedEvents.Select(GetAsEvent);
            return events;
        }

        private object GetAsEvent(ResolvedEvent resolvedEvent)
        {
            var recordedEvent = resolvedEvent.Event;
            var eventName = recordedEvent.EventType;
            var eventDestinationType = GetEventDestinationType(eventName);
            var data = recordedEvent.Data;
            var json = Encoding.UTF8.GetString(data);
            var @event = JsonConvert.DeserializeObject(json, eventDestinationType);
            return @event;
        }

        private Type GetEventDestinationType(string eventName)
        {
            var eventType = _supportedEvents.SingleOrDefault(x => x.Name == eventName);
            if (eventType == null)
            {
                throw new ArgumentException($"{eventName} is not supported");
            }
            return eventType;
        }
    }
}