using System.Collections.Generic;
using System.Threading.Tasks;

namespace EventStoreWrapper.Abstractions
{
    public interface IEventStoreWriter
    {
        Task WriteEvents(string streamName, int aggregateVersion, IEnumerable<object> events);
    }
}