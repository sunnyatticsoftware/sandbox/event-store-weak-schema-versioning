using System.Collections.Generic;
using System.Threading.Tasks;

namespace EventStoreWrapper.Abstractions
{
    public interface IEventStoreReader
    {
        Task<IEnumerable<object>> ReadEvents(string streamName);
    }
}