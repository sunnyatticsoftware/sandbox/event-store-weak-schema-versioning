using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EventStore.ClientAPI;
using EventStoreWrapper.Abstractions;
using Newtonsoft.Json;

namespace EventStoreWrapper
{
    public class EventStoreWriter
        : IEventStoreWriter
    {
        private readonly IEventStoreConnection _eventStoreConnection;

        public EventStoreWriter(IEventStoreConnection eventStoreConnection)
        {
            _eventStoreConnection = eventStoreConnection;
        }
        
        public async Task WriteEvents(string streamName, int aggregateVersion, IEnumerable<object> events)
        {
            if (streamName == null)
            {
                throw new ArgumentNullException(nameof(streamName));
            }

            if (events == null)
            {
                throw new ArgumentNullException(nameof(events));
            }

            var eventsToPersist = events.ToList();
            var numberOfEvents = eventsToPersist.Count;
            if (numberOfEvents == 0)
            {
                return;
            }

            var expectedVersion = GetExpectedVersion(aggregateVersion, numberOfEvents);
            var eventsData = GetEventsData(eventsToPersist);

            await _eventStoreConnection.AppendToStreamAsync(streamName, expectedVersion, eventsData);
        }

        
        private long GetExpectedVersion(in int aggregateVersion, in int numberOfEvents)
        {
            var originalVersion = aggregateVersion - numberOfEvents;
            var expectedVersion = originalVersion == 0 ? ExpectedVersion.NoStream : originalVersion - 1;
            return expectedVersion;
        }
        
        private IEnumerable<EventData> GetEventsData(IEnumerable<object> eventsToPersist)
        {
            var eventsData = eventsToPersist.Select(GetAsEventData);
            return eventsData;
        }

        private EventData GetAsEventData(object eventToPersist)
        {
            var json = JsonConvert.SerializeObject(eventToPersist);
            var data = Encoding.UTF8.GetBytes(json);
            var id = Guid.NewGuid();
            var eventTypeName = eventToPersist.GetType().Name;
            var eventData = new EventData(id, eventTypeName, true, data, null);
            return eventData;
        }
    }
}