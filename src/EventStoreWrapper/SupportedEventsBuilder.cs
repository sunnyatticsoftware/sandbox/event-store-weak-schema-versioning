using System;
using System.Collections.Generic;

namespace EventStoreWrapper
{
    public class SupportedEventsBuilder
    {
        private readonly List<Type> _supportedTypes = new List<Type>();
        public IEnumerable<Type> SupportedEvents => _supportedTypes.AsReadOnly();
        public SupportedEventsBuilder AddSupportedEvent<TEvent>() 
            where TEvent : class
        {
            _supportedTypes.Add(typeof(TEvent));
            return this;
        }
    }
}