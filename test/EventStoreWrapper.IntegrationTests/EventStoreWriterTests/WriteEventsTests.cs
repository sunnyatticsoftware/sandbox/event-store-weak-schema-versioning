using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EventStore.ClientAPI;
using EventStoreWrapper.Abstractions;
using FluentAssertions;
using Sasw.TestSupport;
using Xunit;

namespace EventStoreWrapper.IntegrationTests.EventStoreWriterTests
{
    public static class WriteEventsTests
    {
        public class Given_An_Event_Store_Connection_And_Some_Events_When_Writing
            : Given_WhenAsync_Then_Test
        {
            private IEventStoreWriter _sut;
            private IEventStoreConnection _eventStoreConnection;
            private string _streamName;
            private List<object> _events;
            private int _aggregateVersion;
            private Exception _exception;

            protected override void Given()
            {
                _eventStoreConnection = 
                    EventStoreConnection.Create(
                        EventStoreTestConstants.DefaultConnectionString);
                _eventStoreConnection.ConnectAsync().GetAwaiter().GetResult();
                
                _events =
                    new List<object>
                    {
                        new SampleEvent(),
                        new SampleEvent(),
                        new SampleEvent()
                    };

                _streamName = $"WriterEventsTests{DateTime.UtcNow.Ticks}";
                _aggregateVersion = _events.Count;
                
                _sut = new EventStoreWriter(_eventStoreConnection);
            }

            protected override async Task WhenAsync()
            {
                try
                {
                    await _sut.WriteEvents(_streamName, _aggregateVersion, _events);
                }
                catch (Exception exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Persist_The_Events_Without_Errors()
            {
                _exception.Should().BeNull();
            }

            protected override void Cleanup()
            {
                //_eventStoreConnection.Close();
            }
        }

        class SampleEvent {}
    }
}